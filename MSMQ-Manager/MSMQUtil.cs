﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MSMQ_Manager
{
    class MSMQUtil
    {
        static void Main(string[] args)
        {
            bool flag = false;
            List<string> queuesToWork = new List<string>();
            List<string> machines = new List<string>();
            List<string> machinesFromArgs = new List<string>();
            List<string> roles = new List<string>();
            List<string> rolesFromArgs = new List<string>();


            if (args.Length >= 5)
            {
                if (args[4].ToLower() == "all")
                {
                    flag = true;
                    if (args.Length == 6)
                    {
                        queuesToWork = args[5].Split(',').ToList<string>();
                    }
                }
                else if (args[4].ToLower() == "roles")
                {
                    if (args.Length == 6 || args.Length == 7)
                    {
                        flag = true;
                        if (args.Length == 6)
                        {
                            CsvReader.ReadCSV(ConfigurationSettings.AppSettings["csvPath"], args[1], args[2], "roles", ref roles);
                            rolesFromArgs = args[5].Split(',').ToList<string>();
                            foreach (string role in rolesFromArgs)
                            {
                                if (!roles.Contains(role.ToLower()))
                                {
                                    flag = false;
                                    Console.WriteLine("Roles entered are wrong");
                                    break;
                                }
                            }
                        }
                        else if (args.Length == 7)
                        {
                            CsvReader.ReadCSV(ConfigurationSettings.AppSettings["csvPath"], args[1], args[2], "roles", ref roles);
                            rolesFromArgs = args[5].Split(',').ToList<string>();
                            foreach (string role in rolesFromArgs)
                            {
                                if (!roles.Contains(role.ToLower()))
                                {
                                    flag = false;
                                    Console.WriteLine("Roles entered is wrong");
                                    break;
                                }
                            }
                            if (flag == true)
                            {
                                queuesToWork = args[6].Split(',').ToList<string>();
                            }
                        }
                    }
                }

                else if (args[4].ToLower() == "machines")
                {
                    if (args.Length == 6 || args.Length == 7)
                    {
                        flag = true;
                        if (args.Length == 6)
                        {
                            CsvReader.ReadCSV(ConfigurationSettings.AppSettings["csvPath"], args[1], args[2], "machines", ref machines);
                            machinesFromArgs = args[5].Split(',').ToList<string>();
                            foreach (string machine in machinesFromArgs)
                            {
                                if (!machines.Contains(machine.ToLower()))
                                {
                                    flag = false;
                                    Console.WriteLine("Machines entered are wrong");
                                    break;
                                }
                            }
                        }
                        else if (args.Length == 7)
                        {
                            CsvReader.ReadCSV(ConfigurationSettings.AppSettings["csvPath"], args[1], args[2], "machines", ref machines);
                            machinesFromArgs = args[5].Split(',').ToList<string>();
                            foreach (string machine in machinesFromArgs)
                            {
                                if (!machines.Contains(machine.ToLower()))
                                {
                                    flag = false;
                                    Console.WriteLine("Machines entered are wrong");
                                    break;
                                }
                            }
                            if (flag == true)
                            {
                                queuesToWork = args[6].Split(',').ToList<string>();
                            }
                        }
                    }
                }

            }

            if (flag == false)
            {
                Console.WriteLine("Incorrect arguments. Please enter valid arguments.\n");
                Console.WriteLine("Usage: MSMQ-Manager.exe  Action (pause | resume)");
                Console.WriteLine("                         Env (prod | stage | test)");
                Console.WriteLine("                         Datacenter (iad | chd)");
                Console.WriteLine("                         Cluster (ms02716msmq | (ms02717msmq)");
                Console.WriteLine("                         ServerSet (role(s) | all | machine(s))");
                Console.WriteLine("                         Role/machines (openapi/api/services2b/ms02425) \n");
                Console.WriteLine("                         QueueName(s) (openapi/api/services2b/ms02425) \n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq roles openapi StageLogCounter\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq roles openapi,openapi2 StageLogCounter\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq roles openapi,openapi2\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq machines ms05018 StageLogCounter\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq machines ms05018,ms05019 StageLogCounter\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq machines ms05018,ms05019\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq all StageLogCounter\n");
                Console.WriteLine("i.e  MSMQ-Manager.exe pause prod iad ms02716msmq all\n");
                Console.WriteLine("Press any key to continue.");
                Console.ReadLine();
            }
            else
            {
                string cluster = args[3].ToLower();
                UpdateQueueNames(queuesToWork, cluster);
                List<Dictionary<string, List<string>>> outgoingQueues = new List<Dictionary<string, List<string>>>();
                Dictionary<string, List<string>> serverList = CsvReader.ReadCsv(ConfigurationSettings.AppSettings["csvPath"], args[1].ToLower(), args[2].ToLower());
                List<string> serverstoPull = new List<string>();
                try
                {
                    if (args[4].ToLower() == "roles")
                    {
                        foreach (string role in rolesFromArgs)
                        {
                            serverstoPull = serverList.SingleOrDefault((KeyValuePair<string, List<string>> temprole) => temprole.Key.Equals(role.ToLower(), StringComparison.InvariantCultureIgnoreCase)).Value.ToList<string>();
                            if (queuesToWork.Count > 0)
                            {
                                outgoingQueues.AddRange(MSMQHelper.GetQueueNames(serverstoPull, cluster, queuesToWork));
                            }
                            else
                            {
                                outgoingQueues.AddRange(MSMQHelper.GetQueueNames(serverstoPull, cluster));
                            }
                        }
                    }
                    else if (args[4].ToLower() == "machines")
                    {
                        if (machinesFromArgs.Count > 0)
                        {
                            if (queuesToWork.Count > 0)
                            {
                                outgoingQueues.AddRange(MSMQHelper.GetQueueNames(machinesFromArgs, cluster, queuesToWork));
                            }
                            else
                            {
                                outgoingQueues.AddRange(MSMQHelper.GetQueueNames(machinesFromArgs, cluster));
                            }
                        }
                    }
                    else if (args[4].ToLower() == "all")
                    {
                        foreach (KeyValuePair<string, List<string>> keyvalue in serverList)
                        {
                            if (queuesToWork.Count > 0)
                            {
                                outgoingQueues.AddRange(MSMQHelper.GetQueueNames(keyvalue.Value, cluster, queuesToWork));
                            }
                            else
                            {
                                outgoingQueues.AddRange(MSMQHelper.GetQueueNames(keyvalue.Value, cluster));
                            }
                        }
                    }

                    if (outgoingQueues.Count > 0)
                    {
                        MSMQHelper.PauseOrResumeOutgoingQueues(outgoingQueues, args[0].ToLower() == "pause");
                    }
                    else
                    {
                        Console.WriteLine("Zero queues found");
                    }
                    Console.WriteLine("Execution completed. Press any key to continue.");
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Execution completed. Press any key to continue.");
                    Console.ReadLine();
                }
            }
        }

        private static void UpdateQueueNames(List<string> queuesToWork, string cluster)
        {
            for(int i=0;i<queuesToWork.Count; i++)
            {
                queuesToWork[i] = string.Format(@"DIRECT=OS:{0}\private$\{1}", cluster, queuesToWork[0]);
            }
        }
    }
}