﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MSMQ_Manager
{
    public class CsvReader
    {
        public static Dictionary<string, List<string>> ReadCsv(string path, string host, string dataCenter)
        {
            Dictionary<string, List<string>> serverList = new Dictionary<string, List<string>>(StringComparer.InvariantCultureIgnoreCase);
            try
            {
                StreamReader reader = new StreamReader(File.OpenRead(path));
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line != null)
                    {
                        string[] values = line.Split(',');
                        if (values.Length < 4)
                        {
                            throw new Exception("Invalid csv file. Data is wrong");
                        }
                        if (values[0] != "host")
                        {
                            if (values[1]==host && values[2] == dataCenter)
                            {
                                if (serverList.ContainsKey(values[3]))
                                {
                                    serverList[values[3]].Add(values[0]);
                                }
                                else
                                {
                                    serverList.Add(values[3].ToLower(), new List<string>
                                    {
                                        values[0]
                                    });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred while fetching servers and roles from the csv.");
                Console.WriteLine("Exception Details: {0}", ex.Message);
            }
            return serverList;
        }

        public static void ReadCSV(string path, string host, string dataCenter, string toReturn, ref List<string> rolesormachines)
        {
            try
            {
                StreamReader reader = new StreamReader(File.OpenRead(path));
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line != null)
                    {
                        string[] values = line.Split(',');
                        if (values.Length < 4)
                        {
                            throw new Exception("Invalid csv file. Data is wrong");
                        }
                        if (values[0] != "host")
                        {
                            if (toReturn.ToLower() == "roles")
                            {
                                if (!string.IsNullOrEmpty(values[1]) && !string.IsNullOrEmpty(values[2]) && values[1].ToLower() == host.ToLower() && values[2].ToLower() == dataCenter.ToLower() && !rolesormachines.Contains(values[3].ToLower()))
                                {
                                    rolesormachines.Add(values[3].ToLower());
                                }
                            }
                            if (toReturn.ToLower() == "machines")
                            {
                                if (!string.IsNullOrEmpty(values[1]) && !string.IsNullOrEmpty(values[2]) && values[1].ToLower() == host.ToLower() && values[2].ToLower() == dataCenter.ToLower() && !rolesormachines.Contains(values[3].ToLower()))
                                {
                                    rolesormachines.Add(values[0].ToLower());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred while fetching servers and roles from the csv.");
                Console.WriteLine("Exception Details: {0}", ex.Message);
            }
        }
    }
}
