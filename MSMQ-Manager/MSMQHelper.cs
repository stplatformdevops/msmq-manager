﻿using MSMQ;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Messaging;
using System.Runtime.CompilerServices;

namespace MSMQ_Manager
{
    public static class MSMQHelper
    {
        public static List<Dictionary<string, List<string>>> GetQueueNames(List<string> machineNames, string clustername)
        {
            if (machineNames == null && machineNames.Count == 0)
            {
                throw new ArgumentNullException("machineNames");
            }
            return (from machine in machineNames
                    select MSMQHelper.GetQueueNames(machine, clustername)).ToList<Dictionary<string, List<string>>>();
        }

        public static Dictionary<string, List<string>> GetQueueNames(string machineName, string clustername)
        {
            Dictionary<string, List<string>> outgoingQueuename = new Dictionary<string, List<string>>
            {
                {
                    machineName,
                    new List<string>()
                }
            };
            try
            {
                MSMQApplication msmqApp = new MSMQApplication();
                msmqApp.Machine = machineName;
                object activeQueues = msmqApp.ActiveQueues;
                object[] arrayObject = (object[])activeQueues;
                if (arrayObject!=null && arrayObject.Count() > 0)
                {
                    foreach(object obj in arrayObject)
                    {
                        if(obj.ToString().ToLower().Contains(clustername.ToLower()))
                        {
                            outgoingQueuename[machineName].Add(obj.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred while fetching outgoing queues");
            }
            return outgoingQueuename;
        }

        public static void PauseOrResumeOutgoingQueues(IEnumerable<Dictionary<string, List<string>>> serverQueueList, bool pause = false)
        {
            int counter = 0;
            int success = 0;
            foreach (Dictionary<string, List<string>> server in serverQueueList)
            {
                KeyValuePair<string, List<string>> currentServer = server.First<KeyValuePair<string, List<string>>>();
                foreach (string queue in currentServer.Value)
                {
                    MSMQManagement queueManagement = new MSMQManagement();
                    object machineName = currentServer.Key;
                    object path = queue;
                    object formatObject = null;
                    try
                    {
                        queueManagement.Init(ref machineName, ref formatObject, ref path);
                        MSMQOutgoingQueueManagement outgoing = (MSMQOutgoingQueueManagement)queueManagement;
                        if (!queueManagement.IsLocal)
                        {
                            counter++;
                            if (pause)
                            {
                                outgoing.Pause();
                                success++;
                            }
                            else
                            {
                                outgoing.Resume();
                                success++;
                            }
                            Console.WriteLine("Outgoing queue: {0} on server {1} is {2}", queue, currentServer.Key, pause ? "paused" : "resumed");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error while {0} queue: {1}  on server {2}. Error message: {3}", new object[]
                        {
                            pause ? "pausing" : "resuming",
                            queue,
                            currentServer.Key,
                            ex.Message
                        });
                    }
                }
            }
            Console.WriteLine("Out of {0} queues, {1} {2} successfully.", counter, success, pause ? "paused" : "resumed");
        }

        public static List<Dictionary<string, List<string>>> GetQueueNames(List<string> machineNames, string clustername, List<string> queueNames)
        {
            if (machineNames == null && machineNames.Count <= 0)
            {
                throw new ArgumentNullException("machineNames");
            }
            return (from machine in machineNames
                    select MSMQHelper.GetQueueNames(machine, clustername, queueNames)).ToList<Dictionary<string, List<string>>>();
        }

        public static Dictionary<string, List<string>> GetQueueNames(string machineName, string clustername, List<string> queueNames)
        {
            Dictionary<string, List<string>> outgoingQueuename = new Dictionary<string, List<string>>
            {
                {
                    machineName,
                    new List<string>()
                }
            };
            try
            {
                MSMQApplication msmqApp = new MSMQApplication();
                msmqApp.Machine = machineName;
                object activeQueues = msmqApp.ActiveQueues;
                object[] arrayObject = (object[])activeQueues;
                if (arrayObject != null && arrayObject.Count() > 0)
                {
                    foreach (object obj in arrayObject)
                    {
                        foreach(string queue in queueNames)
                        {
                            if (obj.ToString().ToLower() == queue.ToLower() && obj.ToString().ToLower().Contains(clustername.ToLower()))
                            {
                                outgoingQueuename[machineName].Add(obj.ToString());
                            }
                        }
                    }
                       
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred while fetching outgoing queues");
            }
            return outgoingQueuename;
        }
    }
}
